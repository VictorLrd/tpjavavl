
public class Vente extends Employe {

	public Vente(String nom, String prenom, int age,int salaire) {
		super(nom, prenom, age,salaire);
	}

	@Override
	public int calculerSalaire(int chiffreAffaire) {
			return (int)(chiffreAffaire/1.2)+400;
	}
	
	
}
