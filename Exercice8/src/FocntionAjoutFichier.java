import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.regex.Pattern;



public class FocntionAjoutFichier {


	/*Fonction qui permet l'initialisation de la classe personnel*/

	public void initialisationFichierEmploye(Personnel perso) throws FileNotFoundException {
		String phrase=recuperationLigneFichier("Employe.txt");
		Pattern p = Pattern.compile(";") ;
		String[] salarie=p.split(phrase);
		Employe employe=null;
		try {
			for (int i =  0 ; i < salarie.length ; i+=4) {
				employe= new Employe(salarie[i],salarie[i+1],Integer.parseInt(salarie[i+2]),Integer.parseInt(salarie[i+3]));
				perso.ajouterEmploye(employe);
			}
			String phrase1=recuperationLigneFichier("Manutention.txt");
			salarie=p.split(phrase1);
			Manutention manutentionaire;
			for (int i =  0 ; i < salarie.length ; i+=4) {
				manutentionaire= new Manutention(salarie[i],salarie[i+1],Integer.parseInt(salarie[i+2]),Integer.parseInt(salarie[i+3]));
				perso.ajouterEmploye(manutentionaire);
			}
			String phrase2=recuperationLigneFichier("Vente.txt");
			salarie=p.split(phrase2);
			Vente vendeur;
			for (int i =  0 ; i < salarie.length ; i+=4) {
				vendeur= new Vente(salarie[i],salarie[i+1],Integer.parseInt(salarie[i+2]),Integer.parseInt(salarie[i+3]));
				perso.ajouterEmploye(vendeur);
			}
			String phrase3=recuperationLigneFichier("Representation.txt");
			salarie=p.split(phrase1);
			Representation representant;
			for (int i =  0 ; i < salarie.length ; i+=4) {
				representant= new Representation(salarie[i],salarie[i+1],Integer.parseInt(salarie[i+2]),Integer.parseInt(salarie[i+3]));
				perso.ajouterEmploye(representant);
			}
		}
		catch(IOException e) {
			System.out.println("Erreur lecture"+e.getMessage());		
		}

	}


	/*R�cup�ration des employ�s ligne par ligne*/
	public String recuperationLigneFichier(String typeEmploye) throws FileNotFoundException {
		File fichier;
		String nomFichier=Paths.get(".").toAbsolutePath().normalize().toString()+"\\"+typeEmploye;
		fichier = new File(nomFichier);
		FileReader fr= new FileReader(fichier);
		BufferedReader br = new BufferedReader(fr);
		String phrase="";
		try {
			String line = br.readLine();
			while(line!=null) {
				phrase+=line;
				line=br.readLine();
			}
			br.close();
			fr.close();
		}
		catch(IOException e) {
			System.out.println("Erreur lecture"+e.getMessage());		
		}
		return phrase;
	}

	/*Ajoute un employ� dans le fichier*/
	public void ajouterEmployeFichier() throws IOException {
		Scanner sc = new Scanner(System.in);
		int res;
		try {
			do {
				System.out.println("Faites votre choix !\n1-Cr�er un nouveau vendeur;\n2-Cr�er un nouveau repr�sentant;\n3-Cr�er un nouveau manutentionaire;\n4-Cr�er un nouvel employ�");
				res=sc.nextInt();
			}while(res!=1 && res!=2 && res!=3 && res!=4);
			File fichier;

			switch(res)
			{
			case 1:
				ecritureDansFichier("Vente");
				break;
			case 2:
				ecritureDansFichier("Representation");
				break;
			case 3:
				ecritureDansFichier("Manutention");
				break;
			case 4:
				ecritureDansFichier("Employe");
				break;
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("C'est enregistr� !");
	}

	/*Ecris l'employ� dans le fichier*/
	public void ecritureDansFichier(String typeEmploye) throws IOException {
		String nomFichier=Paths.get(".").toAbsolutePath().normalize().toString()+"\\"+typeEmploye;
		try {
			ecritureCaracteristique(nomFichier);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*Demande toutes les caract�ristiques d'un employ� et les ecris dans le fichier*/
	public void ecritureCaracteristique(String nomFichier) throws IOException {
		Scanner sc = new Scanner(System.in);
		File fichier = new File(nomFichier+".txt");
		FileWriter ecritureNouveauFichier;
		String carac;
		String newline = System.getProperty("line.separator");
		try {
			ecritureNouveauFichier = new FileWriter(fichier,true);
			do {
				System.out.println("1:Entrez son nom:");
				carac=sc.nextLine();
				if(Pattern.matches("[a-zA-Z]*",carac)) {
					ecritureNouveauFichier.write(newline);
					ecritureNouveauFichier.write(carac);
					ecritureNouveauFichier.write(";");
				}			
			}while(!Pattern.matches("[a-zA-Z]*",carac));
			do {
				System.out.println("2:Entrez son pr�nom:");
				carac=sc.nextLine();
				if(Pattern.matches("[a-zA-Z]*",carac)) {
					ecritureNouveauFichier.write(carac);
					ecritureNouveauFichier.write(";");
				}
			}while(!Pattern.matches("[a-zA-Z]*",carac));
			do {
				System.out.println("3:Entrez son age:");
				carac=sc.nextLine();
				if(Pattern.matches("[0-9]*",carac)) {
					ecritureNouveauFichier.write(carac);
					ecritureNouveauFichier.write(";");
				}
			}while(!Pattern.matches("[0-9]*",carac));
			do {
				System.out.println("4:Entrez son salaire:");
				carac=sc.next();
				if(Pattern.matches("[0-9]*",carac)) {
					ecritureNouveauFichier.write(carac);
					ecritureNouveauFichier.write(";");
				}
			}while(!Pattern.matches("[0-9]*",carac));
			sc.close();
			ecritureNouveauFichier.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
