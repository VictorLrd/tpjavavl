
public class Personnel {
	
	private int nbEmploye=0;
	protected Employe employe[]= new Employe[100];
	
	
	
	public Personnel() {
		
	}
	
	public void ajouterEmploye(Employe nouveauEmploye) {
		this.employe[nbEmploye]=nouveauEmploye;
		nbEmploye++;
	}
	
	public int calculerSalaires() {
		int salaireTotal=0;
		for(int i=0;i<nbEmploye;++i) {
			salaireTotal= salaireTotal+employe[i].getSalaire();
		}
		return salaireTotal;
	}
	
	public void afficherSalaires() {
		System.out.println("Le salaire total du personnel est de "+calculerSalaires());
	}
	public double salaireMoyen() {
		return (calculerSalaires()/nbEmploye);
	}
	
	public void afficherSalarie() {
		System.out.println("Liste salari� avec d�tails ( ID , Nom , Prenom , Age , Salaire)");
		for(int i=0;i<nbEmploye;i++) {
			System.out.println(i+": "+employe[i].getNom()+" | "+employe[i].getPrenom()+" | "+employe[i].getAge()+" | "+employe[i].getSalaire());
		}
	}
}
