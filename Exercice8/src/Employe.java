import java.util.Date;


public class Employe implements Salaire {
		private String nom;
		private String prenom;
		private int age;
		private int salaire;
		//private Date dateEntree;
		
		public Employe(String nom, String prenom, int age,int salaire) {
			this.nom = nom;
			this.prenom = prenom;
			this.age = age;
			this.salaire=salaire;
			//this.dateEntree = dateEntree;
		}
		
		public int getAge() {
			return age;
		}

		public void setAge(int age) {
			this.age = age;
		}

		public int getSalaire() {
			return salaire;
		}

		public void setSalaire(int salaire) {
			this.salaire = salaire;
		}

		public String getNom() {
			return (this.nom);
		}

		public String getPrenom() {
			return prenom;
		}

		public void setPrenom(String prenom) {
			this.prenom = prenom;
		}

		public void setNom(String nom) {
			this.nom = nom;
		}

		@Override
		public int calculerSalaire(int chiffreAffaire) {
			return 0;
		}

}