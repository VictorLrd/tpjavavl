import java.util.Scanner;
import java.io.*;

public class EcritureFichier {

	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrez le nom du fichier dans lequel vous souhaitez �crire");
		String nomFichier = sc.nextLine()+".txt";
		File nouveauFichier = new File(nomFichier);
		FileWriter ecritureNouveauFichier;
		String ligne;
		String newline = System.getProperty("line.separator");
		try {
			ecritureNouveauFichier = new FileWriter(nouveauFichier,true);
			System.out.println(
					"Veuillez entrez votre texte ligne par ligne, si vous souhaitez arr�ter entrer la ligne quit");
			ligne = sc.nextLine();
			while (!ligne.equals("quit")){			
				ecritureNouveauFichier.write(ligne);
				ecritureNouveauFichier.write(newline);
				ligne = sc.nextLine();
			}
			System.out.println(
					"Vous avez termin� l'�criture de votre fichier !\nVous pouvez le retrouver au chemin suivant:"
							+ nouveauFichier.getAbsolutePath());
			ecritureNouveauFichier.close();
			sc.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
