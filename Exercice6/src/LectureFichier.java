import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class LectureFichier {

	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(System.in);
		File fichier;
		String nomFichier;
		do {
			System.out.println("Entrez le chemin du fichier que vous souhaitez lire");
			nomFichier = sc.nextLine();
			fichier = new File(nomFichier);
		}while(!fichier.exists());
		System.out.println("Entrez le nom du nouveau fichier:");
		String nomNouveauFichier = sc.nextLine();
		sc.close();
		File nouveauFichier = new File(nomNouveauFichier+".txt");
		FileReader fr= new FileReader(fichier);
		BufferedReader br = new BufferedReader(fr);
		FileWriter ecritureNouveauFichier;
		try {
			ecritureNouveauFichier = new FileWriter(nouveauFichier);
			String line = br.readLine();
			while(line!=null) {
				System.out.println(line);
				ecritureNouveauFichier.write(line);
				line=br.readLine();
			}
			ecritureNouveauFichier.close();
			br.close();
			fr.close();
			System.out.println(
					"Vous avez termin� l'�criture de votre fichier !\nVous pouvez le retrouver au chemin suivant:"
							+ nouveauFichier.getAbsolutePath());
			System.out.println("Fin cr�ation nouveau fichier");
		}
		catch(IOException e) {
			System.out.println("Erreur lecture"+e.getMessage());		
		}
	}
}
