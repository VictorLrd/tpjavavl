import java.io.File;
import java.util.Scanner;

public class LectureRep {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String nomRep = "";
		do {
			System.out.println("Entrez un chemin de répertoire valide:");
			nomRep = sc.nextLine();
		} while (!verifRep(nomRep));
		sc.close();
	}
	
	
	static boolean verifRep(String rep){
		File fichier=new File(rep);
		if(fichier.isDirectory() && fichier.exists()) {
			System.out.println("Le chemin est un répertoire");
			return true;}
		else if(fichier.isFile() && fichier.exists()) {
			System.out.println("Le chemin est un fichier");
			return true;}
		System.out.println("Le chemin n'existe pas");
		return false;
	}	

}
