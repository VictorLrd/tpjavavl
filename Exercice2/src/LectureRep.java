import java.io.File;
import java.util.Scanner;

public class LectureRep {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String nomRep;
		do {
			System.out.println("Entrez un chemin de répertoire valide:");
			nomRep = sc.nextLine();
		} while (!verifRep(nomRep));
		sc.close();
		listeFichiersRep(nomRep);
	}
	
	/*Liste les fichiers dans le répertoire*/
	static void listeFichiersRep(String rep) {
		File fichier = new File(rep);
		File[] fichiers = fichier.listFiles();
		String ext = "";
		String val;
		if (fichiers != null) {
			for (int i = 0; i < fichiers.length; i++) {
				val = fichiers[i].getName();
				if (val.lastIndexOf(".") > 0) {
					ext = val.substring(val.lastIndexOf("."));
					if (!fichiers[i].isDirectory() && ext.equals(".txt"))
						System.out.println("Fichier:" + fichiers[i].getName());
				}
			}
		}
	}

	/*Verifie si c'est un répertoire*/
	static boolean verifRep(String rep) {
		File fichier = new File(rep);
		return (fichier.isDirectory());
	}

}