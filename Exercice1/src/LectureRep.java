import java.io.File;
import java.util.Scanner;

public class LectureRep {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String nomRep;
		do {
			System.out.println("Entrez un chemin de r�pertoire valide:");
			nomRep = sc.nextLine();
		} while (!verifRep(nomRep));
		sc.close();
		listeFichiersRep(nomRep);
	}
	
	/*Liste les fichiers dans le repertoire*/
	static void listeFichiersRep(String rep) {
		File fichier = new File(rep);
		File[] fichiers = fichier.listFiles();
		if (fichiers != null) {
			for (int i = 0; i < fichiers.length; i++) {
				if (!fichiers[i].isDirectory())
					System.out.println("Fichier:" + fichiers[i].getName());
			}
		}
	}

	/*V�rifie si c'est un r�pertoire*/
	static boolean verifRep(String rep) {
		File fichier = new File(rep);
		return (fichier.isDirectory());
	}

}
